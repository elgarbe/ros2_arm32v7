DOCKER_USERNAME ?= elgarbe

all: help

help:
	@echo "# Help Menu"
	@echo "    1. make build            - build all images"
	@echo "    2. make pull             - pull all images"
	@echo "    3. make pull             - push all images"
	@echo "    4. make clean            - remove all images"

build:
	@docker run --rm --privileged multiarch/qemu-user-static --reset --persistent yes --credential yes
	@docker buildx build --platform=linux/arm/v7 -t ${DOCKER_USERNAME}/arm32v7_ros2_build:iron -f docker_ros2_iron/build/Dockerfile docker_ros2_iron/build/
	@docker buildx build --platform=linux/arm/v7 -t ${DOCKER_USERNAME}/arm32v7_ros2:iron -f docker_ros2_iron/ros2_iron-core/Dockerfile docker_ros2_iron/ros2_iron-core/
	@docker buildx build --platform=linux/arm/v7 -t ${DOCKER_USERNAME}/arm32v7_micro-ros-agent:iron -f docker_micro-ros/Dockerfile docker_micro-ros/
	@docker buildx build --platform=linux/arm/v7 -t ${DOCKER_USERNAME}/arm32v7_mavros:iron -f docker_mavros/Dockerfile docker_mavros/
	@docker buildx build --platform=linux/arm/v7 -t ${DOCKER_USERNAME}/arm32v7_ros2:devel -f docker_ros2_iron/ros2_iron-devel/Dockerfile docker_ros2_iron/ros2_iron-devel/

pull:
	@docker pull ${DOCKER_USERNAME}/arm32v7_ros2:iron
	@docker pull ${DOCKER_USERNAME}/arm32v7_micro-ros-agent:iron
	@docker pull ${DOCKER_USERNAME}/arm32v7_mavros:iron
	@docker pull ${DOCKER_USERNAME}/arm32v7_ros2:devel

push:
	@docker push ${DOCKER_USERNAME}/arm32v7_ros2:iron
	@docker push ${DOCKER_USERNAME}/arm32v7_micro-ros-agent:iron
	@docker push ${DOCKER_USERNAME}/arm32v7_mavros:iron
	@docker push ${DOCKER_USERNAME}/arm32v7_ros2:devel

clean:
	@docker rmi -f ${DOCKER_USERNAME}/arm32v7_ros2:iron
	@docker rmi -f ${DOCKER_USERNAME}/arm32v7_micro-ros-agent:iron
	@docker rmi -f ${DOCKER_USERNAME}/arm32v7_mavros:iron
	@docker rmi -f ${DOCKER_USERNAME}/arm32v7_ros2:devel
