numpy==1.24.0
dqrobotics
matplotlib
numpy-quaternion
numba
progressbar2
scipy
transformations