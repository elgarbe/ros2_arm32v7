# Dockerfiles para usar ROS2 en el Odroid del miniASV

En este repo tenemos los Dockerfiles de los diferentes packetes/drivers del sistema.
El Odroid XU4 es mas potente que las raspi 3, posee un octacore vs los quadcore de las raspi. El problema es que es ARMv7 (32 bits) y ROS2 pasó esa arquitecura a Tire 3, por lo que la única alternativa es compilar los fuentes.
Primero se obiene una imagen con ROS2 iron compilado desde los fuentes, utilizando root como usuario. Dicha imagen se construye a partir de otra. En la primera se parte de una imagen base de ubuntu jammy para arm32v7 y si instalan las dependencias para compilar ros2, esta imagen se denomina elgarbe/arm32v7_ros2_build:iron. En la segunda se usa esta imagen como base, se clonan los fuentes que uno quiere (primero se genera el .repo con rosdepinstall_generator) y los compila, de este modo se obtiene una imagen de menor tamaño, denominada elgarbe/arm32v7_ros2:iron. Esta imagen posee un ros2 iron funcional, pero con usuario root.
Esta imagen es utilizada como base para compilar micro-ros-agent por un lado y mavros por el otro y obtener imagenes que encapsulen esos paquetes.

Para trabajar en el odroid con paquetes de ros2 propios en un entorno "devel", es necesario tener un usuario con el mismo UID:GID que el usuario host. Entonces, cree una nueva imagen, partiendo como base de elgarbe/arm32v7_ros2_build:iron y en la cual se crea un usuario no root y se lo utiliza para compilar ros2. Esta imagen puede ser usada como base de otra que monte el workspace local, las librerías de larapi y se utilice como contenedor de desarrollo aprovechando vscode.

En el caso de trabajar en la PC, tengo otro dev-container que parte de una imagen oficial de ros:iron-ros-base.

Como estas cosas fueron armadas sobre la marcha aprendiendo a usar todas las herramientas, quedó un poco desordenado. Yo creo que lo ideal sería cambiar la estructura un poco y hacer que la imagen elgarbe/arm32v7_ros2:iron ya compile los fuentes con el usuario del host y a partir de allí se desprendan las otras imagenes. Como inicialmente no me funcionaba usar un usuario igual al del host, arranqué usando root y por eso no quedó muy bien. ToDo: Arreglar esto

Para la compilación se usa docker buildx, el cual permite cros-compilar imagenes. Utilizando un docker de qemu:
```bash
docker run --rm --privileged multiarch/qemu-user-static --reset --persistent yes --credential yes
```
y luego ejecutando:
```bash
docker buildx build --platform=linux/arm/v7 -t elgarbe/arm32v7_ros2_dev:devel -f Dockerfile .
```
es posible cross compilar la imagen.

## Mavros

Se crea una imagen, partiendo de la previamente generada cn ros2, con mavlink/mavros.

## micro-ros-Agent

La Choriboard v4 corre las librerías de micro-ros sobre FreeRTOS y se comunica con el Odroid por medio de la UART a 921600bps (el USB funciona pero de forma inestable). Dentro de la chori se crea un nodo con varios publishers (IMU, Radio Control, etc.) y un suscriptor a un tópico cmd_pwm

## Compilar y ejecutar

Las imágenes se compilan y suben a dockerhub usando el Makefile.

Para levantar todos los paquetes usamos docker compose. En el mismo configuramos la [restart policy](https://docs.docker.com/config/containers/start-containers-automatically/#use-a-restart-policy) para lanzar los paquetes cuando el Odroid bootea.
```bash
docker compose up -d 
```
y de esa forma conseguimos que micro-ros-agent y mavros arranquen con el booteo del Odroid.
