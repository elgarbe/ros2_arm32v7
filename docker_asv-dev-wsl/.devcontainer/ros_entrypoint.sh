#!/bin/bash

echo "ros_entrypoint"

if [ "$ROS_DISABLE_SHM" = "1" ] ; then
    echo "Deshabilitando DDS SHM"
    export FASTRTPS_DEFAULT_PROFILES_FILE=/tmp/disable_fastdds_shm.xml
fi

exec "$@"