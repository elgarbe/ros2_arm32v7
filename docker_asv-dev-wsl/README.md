# Docker dev container para desarrollar con ROS2 iron en linux

Este repositorio contiene:
Un Dockerfile en el cuál se parte de ros:iron-ros-base y se instalan las dependencias para poder usar colcon y compilar los nodos desarrollados.
En el mismo, también se deshabilita la memoria compartida para que los nodos se comuniquen por UDP aunque parezcan estar en el mismo host (por lo de --network=host). Se crea un usuario con los mismo UID y GID que el usuario actual en el host de modo de poder compartir los archivos sin problemas de paermisos. Finalmente se instalan las dependencias de LARAPI.

Un devcontainer.json, el cuál es utilizado por vscode para establecer la forma de compilar, correr y configurar el contenedor de desarrollo. En el mismo se especifica que se utiliza un docker-compose. También se especifica una acción al finalizar la carga del contenedor, la cuál es la compilación de LARAPI. De esta forma es posible modificar la librería y tenerla compilada cada vez que se arranca el contenedor.

En el docker-compose se configuran un par de volúmenes, correspondientes a LARAPI (la tengo por fuera de este repo, pero creo que podría agregarla como submódulo) y al source del paquete de nodos que hacen las veces de controladores (ASMC y PID).

