#!/bin/bash
set -e

# setup ros2 environment
source "/home/elgarbe/ros2_ws/install/setup.bash"
exec "$@"